import 'package:flutter/material.dart';
import 'package:flutter_video/video_player_widget.dart';

void main() {
  runApp(App());
}

class App extends StatelessWidget {
  const App({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      themeMode: ThemeMode.system,
      home: Scaffold(
        appBar: AppBar(
          title: Text('flutter_video'),
        ),
        body: Center(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              _Button(
                buttonText: 'Network video',
                videoPlayerMode: VideoPlayerMode.network,
                resourcePath:
                    'https://flutter.github.io/assets-for-api-docs/assets/videos/butterfly.mp4',
              ),
              _Button(
                buttonText: 'Asset video',
                videoPlayerMode: VideoPlayerMode.asset,
                resourcePath: 'assets/mp4/mindfulness.mp4',
              ),
            ]
                .map(
                  (e) => Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: e,
                  ),
                )
                .toList(),
          ),
        ),
      ),
    );
  }
}

class _Button extends StatelessWidget {
  final String buttonText;
  final String resourcePath;
  final VideoPlayerMode videoPlayerMode;

  const _Button({
    Key key,
    @required this.buttonText,
    @required this.resourcePath,
    @required this.videoPlayerMode,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return RaisedButton(
      child: Text(buttonText),
      onPressed: () => _pushRoute(
          context,
          VideoPlayerWidget(
            resourcePath: resourcePath,
            videoPlayerMode: videoPlayerMode,
          )),
    );
  }
}

void _pushRoute(BuildContext context, Widget widget) {
  Navigator.push(
    context,
    MaterialPageRoute(builder: (context) {
      return Scaffold(appBar: AppBar(), body: Center(child: widget));
    }),
  );
}
