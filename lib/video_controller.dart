import 'package:flutter/foundation.dart';

abstract class VideoController {
  final String filePath;

  bool isInitialised;

  VideoController({
    @required this.filePath,
    this.isInitialised = false,
  });

  void play();

  void pause();

  void restart();

  void initialise();

  void dispose();
}

class VideoControllerImpl extends VideoController {
  @override
  bool get isInitialised => super.isInitialised;

  @override
  void dispose() {
    // TODO: implement dispose
  }

  @override
  void initialise() {
    // TODO: implement initialise
  }

  @override
  void pause() {
    // TODO: implement pause
  }

  @override
  void play() {
    // TODO: implement play
  }

  @override
  void restart() {
    // TODO: implement restart
  }
}
