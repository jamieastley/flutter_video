import 'package:chewie/chewie.dart';
import 'package:flutter/material.dart';
import 'package:video_player/video_player.dart';

enum VideoPlayerMode { asset, network }

class VideoPlayerWidget extends StatefulWidget {
  final String resourcePath;
  final VideoPlayerMode videoPlayerMode;

  const VideoPlayerWidget({
    Key key,
    @required this.resourcePath,
    this.videoPlayerMode = VideoPlayerMode.asset,
  }) : super(key: key);

  @override
  _VideoPlayerWidgetState createState() => _VideoPlayerWidgetState();
}

class _VideoPlayerWidgetState extends State<VideoPlayerWidget> {
  VideoPlayerController videoPlayerController;
  ChewieController chewieController;

  @override
  void initState() {
    super.initState();
    this._init();
  }

  @override
  void dispose() {
    videoPlayerController.dispose();
    chewieController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: MediaQuery.of(context).size.height / 2,
      child: chewieController != null &&
              chewieController.videoPlayerController.value.initialized
          ? Chewie(
              controller: chewieController,
            )
          : Center(child: CircularProgressIndicator()),
    );
  }

  Future<void> _init() async {
    if (widget.videoPlayerMode == VideoPlayerMode.network) {
      videoPlayerController =
          VideoPlayerController.network(widget.resourcePath);
    } else {
      videoPlayerController = VideoPlayerController.asset(widget.resourcePath);
    }

    await videoPlayerController.initialize();
    chewieController = ChewieController(
      videoPlayerController: videoPlayerController,
      autoPlay: true,
      looping: true,
    );

    setState(() {});
  }
}
